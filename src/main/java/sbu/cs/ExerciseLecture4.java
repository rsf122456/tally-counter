package sbu.cs;

import java.lang.StringBuilder;
import java.util.Scanner;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        int sum = 1;
        if ( n == 0) {
            return 1;
        }
        else{
            for (int i = 1; i <= n; i++){
                sum *= i;
            }
        return sum;
        }

    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        long f1 = 1;
        long f2 = 1;
        long f3 = 0;
        if (n == 1){
            return f1;
        }
        else if (n == 2){
            return f2;
        }
        else{
            for (int i = 2; i < n ; i++){
                f3 = f1 + f2;
                f1 = f2;
                f2 = f3;
            }
            return f2;
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        return new StringBuilder(word).reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String state = line.toLowerCase().replaceAll(" ","");
        String word = new StringBuilder(state).reverse().toString();
        System.out.println(word);
        if (word.equalsIgnoreCase(state))
            return true;
        else
            return false;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {

        char[][] arr = new char[str1.length()][str2.length()];

        for (int i = 0; i < str1.length(); i++){
            for (int j = 0; j < str2.length();  j++){
                if (str1.charAt(i) == str2.charAt(j)){
                    arr[i][j] = '*';
                }
                else
                    arr[i][j] = ' ';
            }
        }

        return arr;
    }
}
