package sbu.cs;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        String secret = "qwertyuioplkjhgfdsazxcvbnm";
        StringBuilder password = new StringBuilder();

        for (int i = 0 ;i < length; i++){
            int randomNum = ThreadLocalRandom.current().nextInt(0, 26);
            password = password.append(secret.charAt(randomNum));
        }

        return password.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if (length < 3)
            throw new IllegalValueException();
        String letters = "qwertyuioplkjhgfdsazxcvbnm9803512647:$*;&/%!@#";
        StringBuilder pass = new StringBuilder(length);
        Random rand = new Random();
        for (int i = 0; i < length; i++) {
            int randompos = rand.nextInt(45);
            pass.append(letters.charAt(randompos));
        }
        String word = pass.toString();
        pass.setLength(0);
        while (!(word.matches(".*[a-z].*") && word.matches(".*[0-9].*") && word.matches(".*[:$*;&/%!@#].*"))){
            for (int i = 0; i < length; i++) {
                int randompos = rand.nextInt(45);
                pass.append(letters.charAt(randompos));
            }
            word = pass.toString();
            pass.setLength(0);
        }
        return word;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {

        long temp = fibonacci(n);
        long f3 = fibonacci(n);
        long sum = 0;
        while (f3 > 2){
            sum += f3 % 2;
            f3 /= 2;
        }
        sum = sum + f3 + temp;
        if (sum == n)
            return true;
        else
            return false;
    }

    public long fibonacci(int n) {
        long f1 = 1;
        long f2 = 1;
        long f3 = 0;
        if (n == 1){
            return f1;
        }
        else if (n == 2){
            return f2;
        }
        else{
            for (int i = 2; i < n ; i++){
                f3 = f1 + f2;
                f1 = f2;
                f2 = f3;
            }
            return f2;
        }
    }
}
