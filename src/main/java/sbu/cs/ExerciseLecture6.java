package sbu.cs;

import java.util.*;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for (int i = 0; i < arr.length; i += 2){
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] reverse = new int[arr.length];
        int index = 0;
        for (int i = arr.length - 1; i > -1; i--){
            reverse[index] = arr[i];
            index++;
        }
        return reverse;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        if (m1[0].length != m2.length)
            throw new RuntimeException();
        double[][] product = new double[m1.length][m2[0].length];
        for (int i = 0; i < m1.length; i++){
            for (int j = 0; j < m2[0].length; j++){
                for (int k = 0; k < m1[0].length; k++){
                    product[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }

        return product;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {

        List<List<String>> myNames = new ArrayList<>();
        for (int i = 0; i < names.length; i++){
            List<String> word = new ArrayList<>();
            for (int j = 0; j < names[i].length; j++){
                word.add(names[i][j]);
            }
            myNames.add(word);
        }
        return myNames;
    }


    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {

        List<Integer> primes = new ArrayList<>();
        int num = 2;
        while (num <= n) {
            boolean Prime = true;
            for (int i = 2; i <= num / 2; i++) {
                if (num % i == 0) {
                    Prime = false;
                    break;
                }
            }
            if (Prime && n % num == 0) {
                primes.add(num);
            }
            num++;
        }

        return primes;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        List<String> words = new ArrayList<>();
        String[] name = line.split(" ");
        String[]  replace = new String[name.length];
        for (int i = 0; i < name.length; i++){
            replace[i] = name[i].replaceAll("[^a-zA-z]","");
            words.add(replace[i]);
        }
        return words;
    }
}
