package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
    int count;

    @Override
    public void count() {
        if (count < 9999)
            count++;
    }

    @Override
    public int getValue() {
        return count;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if (value < 0 || value > 9999)
            throw new IllegalValueException();
        count = value;
    }
}
